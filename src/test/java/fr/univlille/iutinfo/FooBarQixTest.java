package fr.univlille.iutinfo;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FooBarQixTest {
    @Test
    public void should_return_entry_as_string() {
        assertEquals("1", FooBarQix.compute(1));
    }

    @Test
    public void should_return_Foo_when_divisible_by_3() {
        assertEquals("Foo", FooBarQix.compute(6));
    }

    @Test
    public void should_return_Foo_when_divisible_by_5() {
        assertEquals("Bar*", FooBarQix.compute(10));
    }

    @Test
    public void should_return_Qix_when_divisible_by_7() {
        assertEquals("Qix", FooBarQix.compute(14));
    }

    @Test
    public void should_return_combination_of_Foo_Bar_Qix_when_divisible_by_3_5_7() {
        assertEquals("FooBar*", FooBarQix.compute(4*3*5));
        assertEquals("FooQix", FooBarQix.compute(4*3*7));
    }

    @Test
    public void should_replace_3_by_Foo (){
        assertEquals("Foo", FooBarQix.compute(13));
        assertEquals("FooFoo", FooBarQix.compute(3113));
    }

    @Test
    public void should_replace_5_by_Bar (){
        assertEquals("Bar", FooBarQix.compute(5111));
        assertEquals("BarBar", FooBarQix.compute(551));
    }

    @Test
    public void should_replace_7_by_Qix (){
        assertEquals("QixQix", FooBarQix.compute(7724));
    }

    @Test
    public void should_compose_divisible_and_contains (){
        assertEquals("FooBarBar", FooBarQix.compute(15));
    }

    @Test
    public void should_replace_0_by_star(){
        assertEquals("*", FooBarQix.compute(101));
        assertEquals("**", FooBarQix.compute(1004));
        assertEquals("Qix*Foo", FooBarQix.compute(1703));
    }
}