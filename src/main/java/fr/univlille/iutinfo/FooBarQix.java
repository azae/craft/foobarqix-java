package fr.univlille.iutinfo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FooBarQix {
    private static Map<Integer, String> divisorRepresentation = new HashMap<>();
    private static Map<Integer, String> digitsRepresentation = new HashMap<>();

    static {
        divisorRepresentation.put(3, "Foo");
        divisorRepresentation.put(5, "Bar");
        divisorRepresentation.put(7, "Qix");

        digitsRepresentation.putAll(divisorRepresentation);
        digitsRepresentation.put(0, "*");
    }

    public static String compute(Integer entry) {
        if (replaceByDivisible(entry) || replaceByContains(entry))
            return replaceDivisor(entry) + replaceContains(entry);
        return entry.toString();
    }

    private static boolean replaceByContains(Integer entry) {
        long numberOfKey = splitDigits(entry)
                .filter(x -> digitsRepresentation.containsKey(x))
                .count();
        return numberOfKey > 0;
    }

    private static Stream<Integer> splitDigits(Integer entry) {
        return Arrays.stream(entry.toString().split(""))
                .map(Integer::parseInt);
    }

    private static String replaceContains(Integer entry) {
        return splitDigits(entry)
                .map(x -> digitsRepresentation.getOrDefault(x,""))
                .collect(Collectors.joining());
    }

    private static String replaceDivisor(Integer entry) {
        StringBuilder output = new StringBuilder();
        for (Integer divisor : divisorRepresentation.keySet()) {
            if (entry % divisor == 0) output.append(divisorRepresentation.get(divisor));
        }
        return output.toString();
    }

    private static boolean replaceByDivisible(Integer entry) {
        for (Integer divisor : divisorRepresentation.keySet()) {
            if (entry % divisor == 0) return true;
        }
        return false;
    }
}
